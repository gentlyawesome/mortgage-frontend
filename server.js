require('dotenv').config();

const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')
const path = require('path')
const app = express()

const corsOptions = {
  origin: (origin, callback) => {
    callback(null, true);
  },
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
  allowedHeaders: ["Access-Control-Allow-Origin", "Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"],
  credentials: true
};

app.options('*', cors(corsOptions));
app.use(cors(corsOptions));

app.use(express.json({ limit: '100mb'}))
app.use(bodyParser.urlencoded({ extended : true }))
app.use(bodyParser.json())

app.use(cors({
  credentials: true,
  origin: process.env.REACT_APP_API_BASE_URL
}))

app.use(express.static(path.join(__dirname, './build')));

app.get('*', (req,res) => {
  res.sendFile(path.join(__dirname, './build/index.html'));
});

app.listen(process.env.PORT, () => {
  console.log(`Example app listening at http://localhost:${process.env.PORT}`)
});