import axios from 'axios'

const checkAuth = async () => {
    const token = localStorage.getItem('token');

    if(!token){
        return false;
    }

    try{
        const auth = await axios.get(`${process.env.REACT_APP_API_BASE_URL}/check-auth?token=${token}`);
        return auth.data.user;
    }catch(error){
        console.log(error)
    }
    
}

export default checkAuth;
