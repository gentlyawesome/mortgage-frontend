import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Nav from './container/nav';
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import { Fragment, useEffect, useState } from 'react';

import { Home, Dashboard, Signup, Loan } from './container'
import checkAuth from './services/auth';


function App() {
  const [user, setUser] = useState();

  useEffect(() => {
    async function auth(){
      return await checkAuth();
    }
    setUser(auth())
  }, [])

  return (
    <Fragment>
      <Nav />
      <BrowserRouter>
        <Route exact path="/" render={() => (
          user ? (
            <Redirect to="/dashboard" />
          ) : (
            <Route path="/" component={Home} />
          )
        )} /> 
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/signup" component={Signup} />
        <Route path="/loan" component={Loan} />
      </BrowserRouter>
    </Fragment>
  );
}

export default App;
