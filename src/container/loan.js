import axios from "axios";
import { useState } from "react";
import { Row, Col, Container, Form, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";

const Loan = () => {
    const [] = useState(25)
    const { register, handleSubmit, reset }  = useForm();
    const onSubmit = async (data) => {
        const { initialDeposit, monthlyIncome, interest, terms, monthlyExpense } = data;
        try {
            const result = await axios.post(`${process.env.REACT_APP_API_BASE_URL}/calc-mortgage`, { initialDeposit, monthlyIncome, interest, terms,  monthlyExpense });
            console.log({ result: result.data })
        }catch(error){
            console.log(error.response.data)
        }
        reset();
    };

    return (
        <Container>
            <h1>Loan</h1>
            <Row>
                <Col>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Control type="number" placeholder="Initial Deposit" {...register('initialDeposit')} />
                                </Form.Group> 
                                <br />
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Control type="number" placeholder="Monthly Income" {...register('monthlyIncome')} />
                                </Form.Group> 
                                <br />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Control type="number" placeholder="Interest" {...register('interest')} />
                                </Form.Group> 
                                <br />
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Select aria-label="Terms" {...register('terms')}>
                                        <option>Terms</option>
                                        <option value="12">12</option>
                                        <option value="18">18</option>
                                        <option value="24">24</option>
                                        <option value="36">36</option>
                                        <option value="48">48</option>
                                        <option value="60">60</option>
                                    </Form.Select>
                                </Form.Group> 
                                <br />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Control type="number" placeholder="Monthly Expense" {...register('monthlyExpense')} />
                                </Form.Group> 
                                <br />
                            </Col>
                        </Row>
                        <Button variant="primary" type="submit">Create Loan</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Loan;