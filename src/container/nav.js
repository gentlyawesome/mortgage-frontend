import './../App.css' 
import { Container, Navbar } from "react-bootstrap";
import checkAuth from '../services/auth';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

const Nav = (props) => {
    const [user, setUser] = useState();
    const history = useHistory()

    useEffect(() => {
        async function auth(){
        return await checkAuth();
        }
        setUser(auth())
    }, [])

    const handleLogout = () => {
        localStorage.removeItem('token');

        if(!localStorage.getItem('token')){
          history.push('/')
        }
    }


    return (
        <Navbar className="top-nav">
            <Container>
            <Navbar.Brand href="/">Mortgage Panda</Navbar.Brand>
            <Navbar.Toggle />
            {user && 
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        Signed in as: { user.email } 
                    </Navbar.Text>{' '}
                    <Navbar.Text onClick={() => handleLogout()}>
                        Logout
                    </Navbar.Text>
                </Navbar.Collapse>
            }
            </Container>
        </Navbar>
    )
}

export default Nav;