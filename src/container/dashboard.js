import { Col, Container, Row, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const Dashboard = (props) => {
    return (
        <Container>
            <Row>
                <Col>
                    <h1>Dashboard</h1>
                </Col>
                <Col className="justify-content-end">
                    <Link to="/loan">
                        <Button variant="primary" size="lg">New Loan</Button>
                    </Link>
                </Col>
            </Row>
        </Container>
    )
}

export default Dashboard;