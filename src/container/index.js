import Home from './home'
import Dashboard from './dashboard'
import Loan from './loan'
import Signup from './signup'

export { Home, Dashboard, Loan, Signup  }