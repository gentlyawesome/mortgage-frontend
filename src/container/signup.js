import axios from "axios";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { useForm } from 'react-hook-form'
import { useHistory } from "react-router-dom";

const Signup = (props) => {
    const history = useHistory();
    const { register, handleSubmit, reset }  = useForm();
    const onSubmit = async (data) => {
        const { name, email, password } = data;
        try {
            const result = await axios.post(`${process.env.REACT_APP_API_BASE_URL}/register`, { name, email, password });
            localStorage.setItem('token', result.data.user.token )
            history.push('/dashboard')
        }catch(error){
            console.log(error.response.data)
        }
        reset();
    };

    return (
        <Container>
            <Row className="justify-content-md-center">
                <Col md={6}>
                    <h1>Signup</h1>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Name" {...register('name')} />
                        </Form.Group> 
                        <br />
                        <Form.Group>
                            <Form.Control type="email" placeholder="Email" {...register('email')} />
                        </Form.Group> 
                        <br />
                        <Form.Group>
                            <Form.Control type="password" placeholder="Password" {...register('password')} />
                        </Form.Group> 
                        <br />
                        <Button variant="primary" type="submit">Register</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Signup;