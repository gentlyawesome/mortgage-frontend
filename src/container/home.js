import axios from "axios";
import { Container, Row, Col, Form, Button, Image } from "react-bootstrap";
import { useForm } from 'react-hook-form'
import { Link, useHistory } from "react-router-dom";

const Home = (props) => {
    const history = useHistory();
    const { register, handleSubmit, reset }  = useForm();
    const onSubmit = async (data) => {
        const { email, password } = data;
        try {
            const result = await axios.post(`${process.env.REACT_APP_API_BASE_URL}/login`, { email, password });
            localStorage.setItem('token', result.data.user.token )
            history.push('/dashboard')
        }catch(error){
            console.log(error.response.data)
        }
        reset();
    };

    return (
        <Container>
            <Row>
                <Col>
                    <Image src="https://via.placeholder.com/500" fluid />
                </Col>
                <Col md={6}>
                    <h1>Login</h1>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <Form.Group>
                            <Form.Control type="email" placeholder="Email" {...register('email')} />
                        </Form.Group> 
                        <br />
                        <Form.Group>
                            <Form.Control type="password" placeholder="Password" {...register('password')} />
                        </Form.Group> 
                        <br />
                        <Row>
                            <Col>
                                <Button variant="primary" type="submit">Login</Button>
                            </Col>
                            <Col className="justify-content-end">
                                <Link to="/signup">Signup</Link>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Home;